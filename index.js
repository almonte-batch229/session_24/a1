// Exponent Operator
let getCube = 2**3

// Template Literals
console.log(`The cube of 2 is ${getCube}`)

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [block, street, state, zip] = address

console.log(`I live at ${block} ${street}, ${state} ${zip}`)

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {name, species, weight, measurement} = animal

console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number) 
})

// Javascript Classes
class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

let dog1 = new Dog("Jotaro", 3, "Daschund")
let dog2 = new Dog("Jamies", 2, "Siberian Husky")

console.log(dog1)
console.log(dog2)